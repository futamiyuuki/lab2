/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author hhsiung
 *
 */
public class Fahrenheit extends Temperature
{
	private Temperature temp;
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return super.getValue()+"degrees Fahrenheit";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float cels = (float) ((super.getValue() - 32) / 1.8);
		temp = new Celsius(cels);
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stu 
		temp = new Fahrenheit(super.getValue());
		return temp;
	}
}
