/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author hhsiung
 *
 */
public class Celsius extends Temperature
{
	private Temperature temp;
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return super.getValue()+" degrees Celsius";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		temp = new Celsius(super.getValue());
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float fahr = (float) (super.getValue() * 1.8 + 32);
		temp = new Fahrenheit(fahr);
		return temp;
	}
}
